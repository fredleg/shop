SET NAMES utf8;

DROP DATABASE shop;
CREATE DATABASE shop;
USE shop;

DROP TABLE IF EXISTS `products`;
DROP TABLE IF EXISTS `product_category`;

# TRUNCATE TABLE Products // Fait un DELETE sur toute la tables ET régénère l'auto-incrément

CREATE TABLE `products` (
	id int(11) NOT NULL AUTO_INCREMENT,
	category_id INT(3),
	name varchar(50) NOT NULL,
	description text NOT NULL,
	price DECIMAL(11,2),
	picture varchar(50),
	rating DECIMAL(2,1),
	date datetime NOT NULL, # DEFAULT CURRENT_TIMESTAMP
	PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;

# Voir fichier /db/generator.php



CREATE TABLE `product_category` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `label` VARCHAR(45) NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;

INSERT INTO `product_category` (id, label) VALUES
   (  1,  'Fruits'),
   (  2,  'Légumes'),
   (  3,  'Graines'),
   (  4,  'Pates'),
   (  5,  'Salades');


