<?php
    require('php/func.php');
    require('php/header.php');
    require('php/db.php');

    $count      = 0; // nombre de produits trouvés
    $sep        = '/[\s,]+/'; // séparateur de keywords: n'importe quels espaces et la virgule
    $categories = $db->query( 'SELECT id, label FROM product_category ORDER BY label' )->fetchAll();
    $arrSor = [
       [ 'id' => 'name',    'label' => 'Name' ],
       [ 'id' => 'price',   'label' => 'Price' ],
       [ 'id' => 'rating',  'label' => 'Rating' ],
       [ 'id' => 'reviews', 'label' => 'Reviews' ]
    ];
    $arrDir = [
       [ 'id' => 'ASC',  'label' => 'Ascending' ],
       [ 'id' => 'DESC', 'label' => 'Descending' ]
    ];

    //showArray( array_column( $arrSor,1 )  );

    // Valeurs par défaut du formulaire
    $keywords  = [];
    $category  = '';
    $price_min = 20;  //
    $price_max = 80;  // Parceque c'est'c'que ch'fais dans la vie
    $picture   = '0';
    $sort      = 'name';
    $direction = 'ASC';

    //Types de recherches
    $recherche_rapide  = !empty( $_GET['q']);
    $recherche_avancee = isset(  $_GET['category']);

    if ( $recherche_rapide ) {

        $keywords = preg_split($sep, $_GET['q'] );
        
        $query = $db->prepare(
            'SELECT * FROM products WHERE TRUE'
            .str_repeat(' AND CONCAT(name,description) LIKE :key',count($keywords))
            .' ORDER BY name'
        );
        foreach($keywords as $key) $query->bindValue(':key', '%'.$key.'%', PDO::PARAM_STR);
        $query->execute();
        // Résultat de recherche rapide
        $count    = $query->rowCount();
        $products = $query->fetchAll();
    }

    if ( $recherche_avancee ) {

        $keywords  = preg_split( $sep, $_GET['keywords'] );
        $category  = $_GET['category'];
        $price     = explode(',', $_GET['price'] );
        $price_min = $price[0];
        $price_max = $price[1];
        $picture   = (!empty($_GET['picture'])   ? $_GET['picture']   : $picture   );
        $sort      = (!empty($_GET['sort'])      ? $_GET['sort']      : $sort      );
        $direction = (!empty($_GET['direction']) ? $_GET['direction'] : $direction );

        $query = $db->prepare( $sql =
            'SELECT * FROM products WHERE TRUE '
            .str_repeat(' AND CONCAT(name,description) LIKE :key',count($keywords))
            .²(strlen($category)>0,  ' AND category_id = :category')
            .²($picture==1,          ' AND LENGTH(picture)>0')
            .²($price_min>0,         ' AND price >= :price_min')
            .²($price_max<100,       ' AND price <= :price_max')
            .' ORDER BY '.$sort.' '.$direction
        );
        if(strlen($category)>0)    $query->bindValue(':category', intval($category), PDO::PARAM_INT);
        foreach($keywords as $key) $query->bindValue(':key', '%'.$key.'%', PDO::PARAM_STR);
        if($price_min>0)           $query->bindValue(':price_min', intval($price_min), PDO::PARAM_INT);
        if($price_max<100)         $query->bindValue(':price_max', intval($price_max), PDO::PARAM_INT);
        //$query->bindValue(':sort', $sort, PDO::PARAM_STR);
        //$query->bindValue(':direction', $direction, PDO::PARAM_STR);
        $query->execute();
        // Résultat de recherche avancée
        $count    = $query->rowCount();
        $products = $query->fetchAll();
    }

    // Variables de page
    $keywords                 = implode( ' ', $keywords);
    $optionsCategories        = selectOptions( $categories, $category );
    $optionsSort              = selectOptions( $arrSor, $sort );
    $optionsDirection         = selectOptions( $arrDir, $direction );
    $on_a_fait_une_recherche  = !empty( $_GET );
    $on_a_trouve_des_produits = $count > 0;
?>


    <!-- Page Content -->
    <div class="container">

        <div class="row">
            <div class="col-lg-12">

                <h1 class="page-header">Search</h1>

                <form class="search form-inline" method="GET">
                    <div class="form-group">
                        <input type="text" id="keywords" name="keywords" class="form-control" placeholder="Keywords" value="<?=$keywords?>">
                    </div>

                    <div class="form-group">
                        <select id="category" name="category" class="form-control">
                            <option value="">Category</option>
                            <?=$optionsCategories?>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="price">Price</label>
                        0 € <input id="price" name="price" type="text" value="" data-slider-min="0" data-slider-max="100" data-slider-step="1" data-slider-value="[<?= $price_min.','.$price_max ?>]"/> 100 €
                    </div>

                    <div class="form-group">
                        <label class="checkbox-inline">
                            <input type="checkbox" id="picture" name="picture" value="1"<?= attrChecked($picture) ?>> Picture
                        </label>
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-default">
                            <span class="glyphicon glyphicon-search" aria-hidden="true"></span> Search
                        </button>
                    </div>
                </form>

            </div><!-- /.col-lg-12 -->
        </div><!-- /.row : Formulaire pour recherche avancée -->


<?php
    if ( $on_a_fait_une_recherche ) {
    ?>
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header"><?= $count ?> search results</h1>
            </div><!-- /.col-lg-12 -->
        </div><!-- /.row : Affichage du nombre de résultats -->
    <?php
    }

    if ( $on_a_trouve_des_produits ) {
    ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="pull-right">
                    <form class="sort form-inline" method="GET">
                        <div class="form-group">
                            <label for="sort">Sort by</label>
                            <select id="sort" name="sort" class="form-control">
                                <?=$optionsSort?>
                            </select>
                            <select id="direction" name="direction" class="form-control">
                                <?=$optionsDirection?>
                            </select>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-default">
                                <span class="glyphicon glyphicon-sort" aria-hidden="true"></span>
                            </button>
                        </div>
                    </form>
                </div>

            </div><!-- /.col-lg-12 -->
        </div><!-- /.row : Formulaire de tri -->

        <hr>

        <div class="row">
            <div class="col-lg-12">

            <?php
                foreach($products as $product) {
                    echo displayProducts($product,' col-lg-3 col-md-4 col-xs-6 thumb');
                }
            ?>

            </div><!-- /.col-lg-12 -->
        </div><!-- /.row : Affichage des produits -->

    <?php
    }
?>

    </div><!-- /.container -->


<?php
    require('php/footer.php');
?>