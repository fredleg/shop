<?php

###############################################################################
########        Pour le développeur                                    ########
###############################################################################

define('¶',"\n"); // alt+0182

function br() {
    echo'<br>';
}

/*
    Sans cette fonction, l'affichage d'un boolean donne '1' lorsqu'il est true
    et '' lorsqu'il est false
*/
function showBool($bool){
    echo ( $bool ? 'true' : 'false' );
}

/*
    Affichage lisible d'un array
*/
function showArray($array){
    echo ( '<pre>'.print_r($array,true).'</pre>' );
}

function showXMP($txt){
    echo ( '<xmp>'.$txt.'</xmp>' );
}

function alert( $js_msg ) {
    // Exemple: alert( '(182).toString(16)' ); Affiche le décimal 182 en hexadécimal
    //          alert( '"Ǝ".charCodeAt(0)' );
    //          alert( '"👕".charCodeAt(0)' );
    echo '<script>alert('. $js_msg .');</script>';
}
//alert( '"Ŧ".charCodeAt(0)' );

function √(){
    echo "ok";
}
# /x TODO:  $product_full['url'] x/ #
#√()#


###############################################################################
########        Fonctions d'usage général                              ########
###############################################################################

/*
	Fonction qui coupe une chaine en preservant les mots
	et ajoute une chaine à la fin du texte
*/
function cutString($text, $max_length = 0, $end = '...', $sep = '[@]') {

	// Si la variable $max_length est définie, supérieure à 0
	// Et que la longueur de la chaine $text est supérieure à $ max_length
	if ($max_length > 0 && strlen($text) > $max_length) {

		// On insère une chaine dans le texte tous les X caractères sans couper les mots
		$text = wordwrap($text, $max_length, $sep, true);
		// On découpe notre chaine en plusieurs bouts répartis dans un tableau
		$text = explode($sep, $text);

		// On retour le premier element du tableau concaténé avec la chaine $end
		return $text[0].$end;
	}

	// On retourne la chaine de départ telle quelle
	return $text;
}

function tabs( $nbr ) {
    return str_repeat( '    ', $nbr );
}

function toggleNavClassActive( $wanted_name ){
    $script_name = strtolower( $_SERVER['SCRIPT_NAME'] );
    if ( $script_name=='/'.$wanted_name.'.php' ) return ' class="active"';
    return '';
}

function attrActive( $id, $active_id ) {
    return ( $id==$active_id ? ' active' : '' );
}

function attrChecked( $value, $checkedValue='1' ){
    return ( $value== $checkedValue ? ' checked' : '' );
}

function selectOptions( $rows, $selected_id=false ){
/*
    $categories = $db->query( 'SELECT id, label FROM product_category ORDER BY label' )->fetchAll();
    $htmlCategories = selectOptions( $categories, 2 );
    showXMP($htmlCategories);
*/
    $html = '';
    foreach($rows as $row) {
        $value = $row['id'];
        $label = $row['label'];
        $selected = ( !empty($selected_id) && $value==$selected_id ? ' selected' : '' );
        $html .= '<option value="'.$value.'"'.$selected.'>'.$label.'</option>'."\n";
    }
    return $html;
}

function ²( $condition, $valeur ){
    if ($condition) return $valeur;
}

###############################################################################
########        Fonctions pour produits et catégories                  ########
###############################################################################

function htmlStarsRating( $rating, $showValue=false ) {
	$html = '';
	$html .= '				<!-- htmlStarsRating -->';
                                for($i=1; $i<round($rating,0)+1;$i++){
                                    $html .= '<span class="glyphicon glyphicon-star"></span>'."\n";
                                }
                                for($i=$i; $i<6; $i++){
                                    $html .= '<span class="glyphicon glyphicon-star-empty"></span>'."\n";
                                }
                                $html .= ( $showValue? '&nbsp;&nbsp;'.$rating.'&nbsp;stars' : '');
    $html .= '				<!-- htmlStarsRating -->';
	return $html;
}


function pathPicture( $path, $sizeName ) {
    if (strlen($path)>0) {
        if (!file_exists('img/product/'.$path)){
           if ($sizeName=='little') return 'defaults/product_little.png';
           if ($sizeName=='big')    return 'defaults/product_big.png';
        }
        return 'product/'.$path;
    } else {
           if ($sizeName=='little') return 'defaults/product_little.png';
           if ($sizeName=='big')    return 'defaults/product_big.png';
    }

}

function displayProducts( $product, $option ='' ) {
?>
                    <div class="product <?= $option ?>">
                        <div class="thumbnail">
                            <img src="/img/<?= pathPicture($product['picture'],'little') ?>" alt=""><!-- src="http://placehold.it/320x150" -->
                            <div class="caption">
                                <h4 class="pull-right"><?= $product['price'] ?> €</h4>
                                <h4><a href="product.php?id=<?=$product['id']?>"><?= cutString($product['name'],20) ?></a>
                                </h4>
                                <p><?= cutString($product['description'],100) ?></p>
                            </div>
                            <div class="ratings">
                                <p class="pull-right">15 reviews</p>
                                <p>
                                <?= htmlStarsRating( $product['rating'] ); ?>
                                </p>
                            </div>
                            <div class="btns clearfix">
                                <a class="btn btn-info pull-left" href="product.php?id=<?=$product['id']?>"><span class="glyphicon glyphicon-eye-open"></span> View</a>
                                <a class="btn btn-primary pull-right" href="product.php?id=<?=$product['id']?>"><span class="glyphicon glyphicon-shopping-cart"></span> Add to cart</a>
                            </div>
                        </div><!-- /.thumbnail -->
                    </div><!-- /.product -->
<?php  
}
