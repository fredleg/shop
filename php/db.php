<?php

define('HOST', 'localhost');
define('USER', 'root');
define('DB', 'shop');

$inc = $_SERVER['DOCUMENT_ROOT'].'/php/db_pwd.php';
$ok = file_exists($inc) && is_readable($inc);
if ($ok) {
    require($inc);
} else {
	define('PASS', '');
}

try {

    global $db;
    $db_options = array(
            PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING
    );

    $db = new PDO('mysql:host='.HOST.';dbname='.DB.'', USER, PASS, $db_options);

} catch (Exception $e) {
    exit('MySQL Connect Error >> '.$e->getMessage());
}


