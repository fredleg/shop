
                <p class="lead">Categories</p>

                <div class="list-group">

<?php
    $isPageProduct   = isset( $id );
    $cat_to_activate = ( $isPageProduct ? $product_category_id : $catid );

    $query = $db->prepare("SELECT * FROM product_category ORDER BY label");
    $query->execute();
    $categories = $query->fetchAll();

    foreach($categories as $key => $category) {
        $cat_id  = $category['id'];
        $label   = $category['label'];
        $active  = attrActive( $category['id'], $cat_to_activate );

        // Affichage et activation des catégories
        echo tabs(5).'<a href="/?catid='.$cat_id
                    .'" class="list-group-item'.$active.'">'
                    .$label.'</a>'.¶;
    }

    if ( $isPageProduct || !empty($catid)) {

        // Affichage du lien vers toutes les catégories
        echo tabs(5).'<a href="/" class="list-group-item"><i>toutes</i></a>'.¶;
    }
?>

                </div>
