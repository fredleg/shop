<?php

	$isPageProduct = isset( $id );

	if ( $isPageProduct ) {
		$titre = 'Related products';
	    $query = $db->prepare(
	        'SELECT * FROM products WHERE id != :id'
	        .²(!empty($catid), ' AND category_id = :catid')
	        .'  AND LENGTH( picture ) > 0'
	        .' ORDER BY date LIMIT 2'
	    );
	    $query->bindValue(':id', intval($id), PDO::PARAM_INT);
	    if(!empty($catid)) $query->bindValue(':catid', intval($catid), PDO::PARAM_INT);
	    $query->execute();
	    $side_products = $query->fetchAll();
	} else {
		$titre = 'Featured products';
	    $query = $db->prepare(
		  'SELECT * FROM products WHERE LENGTH( picture ) > 0'
		  .' ORDER BY rating DESC LIMIT 20'
	    );
	    $query->execute();
	    $products_tmp = $query->fetchAll();
		$side_products = array_rand ( $products_tmp, 4 );
		foreach($side_products as $key => $tmpkey) $side_products[$key] = $products_tmp[$tmpkey];
	}

	// Affichage des produits
    echo '<p class="lead">'.$titre.'</p>';
	foreach($side_products as $key => $product) {
	    echo displayProducts($product);
	}
