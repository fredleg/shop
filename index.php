<?php
    require('php/func.php');
    require('php/header.php');
    require('php/db.php');

    $catid = empty($_GET['catid']) ? false : $_GET['catid'];

    $query = $db->prepare(
        'SELECT * FROM products'
        .²(!empty($catid), ' WHERE category_id = :catid')
        .' ORDER BY date LIMIT 9'
    );
    if(!empty($catid)) $query->bindValue(':catid', intval($catid), PDO::PARAM_INT);
    $query->execute();
    $products = $query->fetchAll();

?>

    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <?php  require('php/sidebar.php');  ?>

            <div class="col-md-9">

                        <?php  require('php/carousel.html');  ?>

                <div class="row">

<?php
    foreach($products as $product) {
        echo displayProducts($product,' col-sm-4 col-lg-4 col-md-4');
    }
?>
                </div><!-- /.row -->

            </div><!-- col-md-9 -->

        </div><!-- /.row -->

    </div><!-- /.container -->


<?php
    require('php/footer.php');
?>