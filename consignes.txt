
► 1. Faire la découpe des templates html
---> les éléments sont placés dans le dossier php, seules les pages restent dans la racine


► 2. Afficher l'année courante dans le footer
---> Copyright © Shop <?= date('Y') ?>. All right reserved.


► 3. Créer une base de données "shop"
---> dossier db : fichier creation.sql


► 4. Créer une table "products" avec les colonnes
	- id
	- category_ INT(3),
	- name,
	- description,
	- price DECIMAL(11,2),
	- picture,
	- rating DECIMAL(2,1),
	- date
---> fichier creation.sql
---> > cd C:\xampp\htdocs\web\shop>
	 > c:\xampp\mysql\bin\mysql -uroot
	 mysql> SOURCE db/creation.sql;


► 5. Insérer du contenu dans la table "products"
	► BONUS : Adapter un script *_generator.php pour automatiser l'insertion du contenu
---> executer /db/generator.php pour voir un produit
---> executer /db/generator.php?ACTION pour remplir la table products
---> une table de 12 catégories est créée (db/creation.sql) et le champ productes.category qui est un id contient un rand(1,12)


► 6. Dans index.php :
	► Afficher les 6 produits les plus récents
---> 'SELECT * FROM productes ORDER BY date DESC LIMIT 6'
---> function displayProducts($product)
---> /php/func.php : function htmlStarsRating( $rating )

	► BONUS : Afficher 2 produits aléatoires parmi les mieux notés dans la sidebar
---> /php/sidebar_products.php
---> function displayProducts($product,$option='') On ajoute un argument pour modifier les class CSS
---> 'SELECT * FROM products ORDER BY rating DESC LIMIT 2' : biens notés mais pas de hasard
	 $choice = array_rand ( $products, 2 ) : prendra 2 au hasard après un LIMIT 20
...> Il faudrait que ces deux rand ne soient pas déjà parmis les produits du corps de la page.
---> Tant qu'on est dans le sidebar j'affiche les 5 catégories qui contiennent le plus de produits bien notés



► 7. Dans product.php :
	► Afficher 1 produit par rapport à un identifiant passé en paramètre
---> 'SELECT * FROM products WHERE id=:id'

	► BONUS : Afficher 3 produits associés dans la sidebar
---> Passage de paramètres (variables globales $g_) à la sidebar.php afin de gérer des affichages différents lorsqu'elle est appelée par index.php ou pas product.php
XXX> catégories class="active" à gérer
XXX> requête pour "produits associés" pas faite

8. Dans les listes de produits (index.php, search.php, ...etc) :
	► N'afficher qu'un extrait de la description du produit
---> En fait c'est déjà fait dans /php/func.php function displayProducts avec la function cutString vue en cours
	► Renvoyer vers la page product.php pour afficher le produit en entier
---> En fait c'est déjà fait dans /php/func.php function displayProducts avec la function cutString vue en cours


9. Dans search.php :
	► Récupérer les données du formulaire de recherche rapide dans le header
---> voir fichier /db/search.php
	► Faire la requête qui va chercher les produits correspondants à la recherche
---> voir fichier /db/search.php


► 10. BONUS : Gérer la navigation en affichant la page active
---> fait dans /php/nav.php
	 et plusieurs pages ont dûes être renommées pour la cohérence
	 et j'ai pris le parti que le nom du site est toujours blanc


► 11. BONUS : Gérer la recherche avancée
---> La présence de la case à cocher Picture indique qu'il peut y avoir des produits sans images, donc:
	 - modification du fichier /db/generator.php
	 - ajout de la fonction pathPicture dans /php/func.php


12. BONUS : Gérer les catégories de produits :
	► Création de table product_category avec les colonnes id, label
---> voir fichier /db/creation.sql
	► Passer par le moteur de recherche avancée pour filtrer sur une catégorie de produit

BONUS
---> Ajout d'un fichier .gitignore pour empêcher le partage du mot de passe de la base de donnée
---> On garde les fichies html d'origine dans /zip_html pour le serveur dev
     ou le fichier /zip_html.maff qui est 'navigable' dans Firefox avec l'extension Mozill Archive Format
---> $ git rm --cached '*/php_errors.log' php_errors.log
	 et ajout de la ligne php_errors.log dans .gitignore