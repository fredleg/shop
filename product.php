<?php
    require('php/func.php');
    require('php/header.php');
    require('php/db.php');


    $id    = empty($_GET['id'])    ? false : intval($_GET['id']);
    $catid = empty($_GET['catid']) ? false : intval($_GET['catid']);

    $query = $db->prepare( 'SELECT * FROM products WHERE id=:id' );
    $query->bindValue(':id', $id, PDO::PARAM_INT);
    $query->execute();

    // Résultat de fiche produit
    $count               = $query->rowCount();
    $product_full        = $query->fetch();
    $product_category_id = $product_full['category_id'];

    // Variables de page
    $img_src     = '/img/'.pathPicture($product_full['picture'],'big');
    $price       = $product_full['price'];
    $name        = $product_full['name'];
    $description = $product_full['description'];
    $brand_url   = 'http://www.brand.com/product_full'; //product_full['brand_url'];
    $starsRating = htmlStarsRating( $product_full['rating'], true )
?>

    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <?php  require('/php/sidebar.php');  ?>

            <div class="col-md-9">

                <?php if($count>0): ?>

                <div class="product-full">
                    <div class="thumbnail">

                        <img class="img-responsive" src="<?=$img_src?>" alt="">
                        <div class="caption-full">
                            <h4 class="pull-right"><?=$price?>&nbsp;€</h4>
                            <h4><a href="#"><?=$name?></a>
                            </h4>
                            <?=$description?>
                            <br><br>
                            <p>See more product_full infos at <a target="_blank" href="#"><?=$brand_url?></a>.</p>
                        </div>
                        <div class="ratings">
                            <p class="pull-right">3 reviews</p>
                            <p>
                                <?=$starsRating?>
                            </p>
                            <input type="number" value="3.3" class="rating"/>
                        </div>
                        <div class="btns text-center clearfix">
                            <a class="btn btn-success" href=""><span class="glyphicon glyphicon-shopping-cart"></span> Add to cart</a>
                        </div>
                    </div><!-- /.thumbnail -->
                </div><!-- /.product-full : La fiche produits -->

                <div class="well">
                    <div class="text-right">
                        <a class="btn btn-primary">Leave a Review</a>
                    </div>

                    <hr>

                    <div class="row">
                        <div class="col-md-12">
                            <span class="glyphicon glyphicon-star"></span>
                            <span class="glyphicon glyphicon-star"></span>
                            <span class="glyphicon glyphicon-star"></span>
                            <span class="glyphicon glyphicon-star"></span>
                            <span class="glyphicon glyphicon-star-empty"></span>
                            Anonymous
                            <span class="pull-right">10 days ago</span>
                            <p>This product was great in terms of quality. I would definitely buy another!</p>
                        </div>
                    </div><!-- /.row -->

                    <hr>

                    <div class="row">
                        <div class="col-md-12">
                            <span class="glyphicon glyphicon-star"></span>
                            <span class="glyphicon glyphicon-star"></span>
                            <span class="glyphicon glyphicon-star"></span>
                            <span class="glyphicon glyphicon-star"></span>
                            <span class="glyphicon glyphicon-star-empty"></span>
                            Anonymous
                            <span class="pull-right">12 days ago</span>
                            <p>I've alredy ordered another one!</p>
                        </div>
                    </div><!-- /.row -->

                    <hr>

                    <div class="row">
                        <div class="col-md-12">
                            <span class="glyphicon glyphicon-star"></span>
                            <span class="glyphicon glyphicon-star"></span>
                            <span class="glyphicon glyphicon-star"></span>
                            <span class="glyphicon glyphicon-star"></span>
                            <span class="glyphicon glyphicon-star-empty"></span>
                            Anonymous
                            <span class="pull-right">15 days ago</span>
                            <p>I've seen some better than this, but not at this price. I definitely recommend this item.</p>
                        </div>
                    </div><!-- /.row -->
                </div><!-- /.well : Les commentaires -->

                <?php else: ?>

                <div class="no-product">
                    <div class="row">
                        <div class="col-md-12">
                        Pas de produit.
                        </div>
                    </div>
                    <?php
                        head('Location: /');
                        exit;
                    ?>
                </div><!-- /.no-product : Message si pas de produits -->

                <?php endif ?>

            </div><!-- /.col-md-9 -->

        </div><!-- /.row -->

    </div><!-- /.container -->


<?php
    require('php/footer.php');
?>