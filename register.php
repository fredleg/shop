<?php
    require('php/func.php');
    require('php/header.php');
?>

    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <div class="page-header">
                <h1 id="type">Register</h1>
            </div>

            <form class="form-horizontal">
                <fieldset>

                    <div class="form-group">
                        <label for="inputEmail" class="col-lg-2 control-label">Email</label>
                        <div class="col-lg-10">
                            <input class="form-control" id="inputEmail" placeholder="Email" type="text">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputPassword" class="col-lg-2 control-label">Password</label>
                        <div class="col-lg-10">
                            <input class="form-control" id="inputPassword" placeholder="Password" type="password">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox"> Remember me
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-lg-10 col-lg-offset-2">
                            <button type="reset" class="btn btn-default">Cancel</button>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>

                </fieldset>
            </form>

        </div><!-- /.row -->

    </div><!-- /.container -->


<?php
    require('php/footer.php');
?>